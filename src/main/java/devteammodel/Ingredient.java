/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package devteammodel;

/**
 *
 * @author User
 */
public class Ingredient {
    private String code;
    private String name;
    private double min;
    private double balance;
    private String unit;
    private double price;

    public Ingredient(String code, String name, double min, double balance, String unit, double price) {
        this.code = code;
        this.name = name;
        this.min = min;
        this.balance = balance;
        this.unit = unit;
        this.price = price;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public double getMin() {
        return min;
    }

    public double getBalance() {
        return balance;
    }

    public String getUnit() {
        return unit;
    }

    public double getPrice() {
        return price;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMin(double min) {
        this.min = min;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Ingredient{" + "code=" + code + ", name=" + name + ", min=" + min + ", balance=" + balance + ", unit=" + unit + ", price=" + price + '}';
    }
    
    
    
}
